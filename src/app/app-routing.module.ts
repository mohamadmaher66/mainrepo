import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [  
  { path: '', loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule) },
  { path: 'home', loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule) },
  { path: 'admin-panel', loadChildren: () => import('./components/admin-panel/admin-panel.module').then(m => m.AdminPanelModule) },
  { path: 'about', loadChildren: () => import('./components/about/about.module').then(m => m.AboutModule) },
  { path: 'contact-us', loadChildren: () => import('./components/contact-us/contact-us.module').then(m => m.ContactUsModule) },
  { path: 'not-found-page', loadChildren: () => import('./components/not-found-page/not-found-page.module').then(m => m.NotFoundPageModule) },
  {path: '**', redirectTo: 'not-found-page'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
