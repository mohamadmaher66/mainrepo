import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ContactUs } from '../contact-us/model/contact-us.model';
import { ContactUsService } from '../contact-us/service/contact-us.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html'
})
export class AdminPanelComponent implements OnInit {

  contactUsList = new Array<ContactUs>();
  displayedColumns: string[] = ['firstName', 'lastName', 'age', 'message', 'creationDate'];
  dataSource = new MatTableDataSource(this.contactUsList);

  constructor(private contactUsService:ContactUsService) { }

  ngOnInit(): void {
    this.contactUsService.getAll().subscribe(
      response => this.getAllOnSuccess(response),
      error => this.getAllOnError(error)
    )
  }

  private getAllOnSuccess(response: any){
    this.contactUsList = response as Array<ContactUs>;
    this.dataSource = new MatTableDataSource(this.contactUsList);
  }
  private getAllOnError(response: any){
    
  }

}
