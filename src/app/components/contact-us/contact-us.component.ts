import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContactUs } from './model/contact-us.model';
import { ContactUsService } from './service/contact-us.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html'
})
export class ContactUsComponent implements OnInit {

  contactUsModel = new ContactUs();
  successMsg: string;

  constructor(private contactUsService: ContactUsService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  submit(contactUsFrom: NgForm){
    if(contactUsFrom.invalid){
      this._snackBar.open("Please fill form fields",null, {
        duration: 2000,
      });
      return;
    }

    this.contactUsModel.creationDate = new Date();
    this.contactUsService.insert(this.contactUsModel).subscribe(
      res => this.onInsertSuccess(res),
      err => this.onInsertError(err)
    );
  }

  private onInsertSuccess(res: any){
    this.successMsg = "Message sent successfully";
    this._snackBar.open("Message sent successfully",null, {
      duration: 2000,
    });
    this.contactUsModel = new ContactUs();
  }
  
  private onInsertError(res: any){
    this._snackBar.open("Message not sent. Please try again later", null, {
      duration: 2000,
    });
    console.log(res);
  }
}
