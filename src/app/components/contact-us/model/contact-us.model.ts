export class ContactUs {
    public firstName: string;
    public lastName: string;
    public age: number;
    public creationDate: Date;
    public message: string;
}