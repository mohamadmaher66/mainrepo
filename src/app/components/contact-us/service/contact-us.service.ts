import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { ContactUs } from '../model/contact-us.model';

@Injectable({
  providedIn: 'root'
})

export class ContactUsService {

  constructor() { }

  public insert(contactUsObject: ContactUs) : Observable<any> {    
    return new Observable((observer) => { 
      let data = localStorage.getItem('contactUsList');
      let contactUsList = new Array<ContactUs>();

      if(data != null && data != ""){
        contactUsList = JSON.parse(data) as ContactUs[];      
      }
      contactUsList.push(contactUsObject);
      localStorage.setItem('contactUsList', JSON.stringify(contactUsList));
      observer.next();
    }); 
  }

  public getAll() : Observable<any> {    
    return new Observable((observer) => { 
      let data = localStorage.getItem('contactUsList');
      let contactUsList = new Array<ContactUs>();

      if(data != null){
        contactUsList = JSON.parse(data) as ContactUs[];      
      }
      observer.next(contactUsList);
    }); 
  }
}
